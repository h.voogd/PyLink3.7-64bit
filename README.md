# PyLink3.7 64bit

PyLink (Python Package for SR-Research Eyelink users). 64 bits version for 64-bit Python 3.7.

Note: pip install pylink will *NOT* install the Eyelink libraries.

Note2: The download section from the SR-Research is outdated. You cannot download a working version from there. You have to request a version for your system at their support. Or download it from here.

Usage: Download and unzip the file, inside find the pylink folder and copy that folder into you Python 3.7 64-bits lib/site-packages folder.
